<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./css/bootstrap.css">
  <link rel="stylesheet" href="./css/bootstrap-grid.css">
  <link rel="stylesheet" href="./libs/fontawesome-free-6.2.0-web/css/fontawesome.css">
  <link rel="stylesheet" href="./libs/fontawesome-free-6.2.0-web/css/fontawesome.min.css">
  <link rel="stylesheet" href="./libs/fontawesome-free-6.2.0-web/css/all.css">
  <link rel="stylesheet" href="./css/style.css">
  <title>Travelgo</title>
</head>

<body class="bg-abu">
  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-abu py-4 sticky-top shadow">
    <div class="container">
      <a class="navbar-brand text-black font-weight-bold " href="#">Travelgo</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link mx-3 font-weight-bold" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link mx-3 " href="#">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mx-3" href="#">Pelayanan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mx-3" href="#">Kontak</a>
          </li>
        </ul>
        <div>
          <button class="btn bg-login text-white">Login</button>
        </div>
      </div>
    </div>
  </nav>
  <!-- End navbar -->

  <!-- Hero Section -->
  <section class="jumbotron px-3 py-4 bg-abu">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-sm-5">
          <h1 class="ukuran">Jelajahi tujuan anda bersama Travelgo</h1>
          <p class="font-weight-light">Memberi anda banyak tempat untuk dikunjungi, kami akan memastikan anda puas dengan tujuan Anda</p>

          <button class=" btn bg-white shadow rounded-pill ">
            <div class="col-sm-3 ">
              <div class="d-flex flex-coulmn2 p-1">
                <div class="d-flex flex-row mx-n2">
                  <div class="rounded inline-block m-auto">
                    <img src="./icons/lokasi.svg">
                  </div>
                  <div class="inline-block">
                    <p class="bg-muda">Lokasi</p>
                    <p class="text-black font-weight-bold">Semarang,Indonesia</p>
                  </div>
                </div>
                <div class="d-flex flex-row inline-block ml-5">
                  <div class="rounded inline-block m-auto p-3">
                    <img src="./icons/tanggal.png">
                  </div>
                  <div class="inline-block">
                    <p class="bg-muda">Tanggal</p>
                    <p class="text-black font-weight-bold ml-n3">11 Juli 2022</p>
                  </div>
                </div>
              </div>
            </div>
          </button>
        </div>
        <div class="col-sm-7 d-none d-sm-block">
          <img src="./images/main.png" class="img-fluid mb-n4 mt-n4" width="100%" alt="">
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero Section -->

  <!-- Layanan Kami -->
  <section class="py-4 px-2">
    <div class="container">
      <div class="d-flex flex-column align-items-center ">
        <h2 class="text-black mt-4">Layanan Kami</h2>
        <p class="text-black mx-4">Travelgo siap membantu perjalanan anda kapan pun dan di mana pun</p>
        <div class="container ">
          <div class="row mt-3">
            <div class="col-4">
              <div class="d-flex flex-column align-items-center shadow bg-putih">
                <img src="./icons/hotel.svg" class="background-orange rounded-circle p-2 border bg-blue mt-3" alt="">
                <div class="media-body d-flex flex-column align-items-center mt-3">
                  <p class="text-black">Hotel</p>
                  <div class="keterangan align-items-center">
                    <p class="text-black p-3">Travelgo menyediakan layanan untuk memesanan hotel sesuai yang anda inginkan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="d-flex flex-column align-items-center shadow bg-putih rounded-3">
                <img src="./icons/pesawat.svg" class="background-orange rounded-circle p-n1 border bg-blue mt-3" alt="">
                <div class="media-body d-flex flex-column align-items-center mt-3">
                  <p class="text-black">Pesawat</p>
                  <div class="keterangan align-items-center">
                    <p class="text-black p-3">Travelgo menyediakan layanan untuk memesanan tiket pesawat sesuai yang anda inginkan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="d-flex flex-column align-items-center shadow bg-putih">
                <img src="./icons/mobil.png " class="background-orange rounded-circle p-2 border bg-blue mt-3" alt="">
                <div class="media-body d-flex flex-column align-items-center mt-3">
                  <p class="text-black">Mobil</p>
                  <div class="keterangan align-items-center">
                    <p class="text-black p-3">Travelgo menyediakan layanan untuk memesanan mobil sesuai yang anda inginkan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-4">
                <div class="d-flex flex-column align-items-center shadow bg-putih ml-3 mr-n1">
                  <img src="./icons/apartemen.png" class="background-orange rounded-circle p-2 border bg-blue mt-3" alt="">
                  <div class="media-body d-flex flex-column align-items-center mt-3">
                    <p class="text-black">Apartemen</p>
                    <div class="keterangan align-items-center ">
                      <p class="text-black p-3 ">Travelgo menyediakan layanan untuk memesanan apartemen sesuai yang anda inginkan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-4">
                <div class="d-flex flex-column align-items-center shadow bg-putih">
                  <img src="./icons/kereta.png" width="70px" class="background-orange rounded-circle border bg-blue mt-4 p-1" alt="">
                  <div class="media-body d-flex flex-column align-items-center mt-3">
                    <p class="text-black">Kereta</p>
                    <div class="keterangan align-items-center">
                      <p class="text-black p-3">Travelgo menyediakan layanan untuk memesanan tiket kereta sesuai yang anda inginkan </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-4">
                <div class="d-flex flex-column align-items-center shadow bg-putih">
                  <img src="./icons/bus.png " class="background-orange rounded-circle border bg-blue mt-3 p-1" alt="">
                  <div class="media-body d-flex flex-column align-items-center mt-3">
                    <p class="text-black">Bus</p>
                    <div class="keterangan align-items-center">
                      <p class="text-black p-3">Travelgo menyediakan layanan untuk memesanan tiket bus sesuai yang anda inginkan </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Layanan Kami -->

            <!-- Footer -->
            <section class="bg-blue mt-4 py-4 ml-3 mb-n4">
              <div class="container">
                <div class="row align-items-center">
                  <div class="col-md-3">
                    <p class="footer-bawah ml-5">Travelgo</p>
                    <p class="text-white my-4 ml-4">Travelgo VR,410-555 Hostings
                      St, Vanocover,BC WEB Indonesia</p>
                    <p class="text-white ml-4">admin@gmail.com</p>
                  </div>
                  <div class="col-sm-2">
                    <div class="d-flex flex-column my-2 mx-2">
                      <div class="d-flex flex-row">
                        <div class="inline-block mx-3">
                          <p class="text-white font-weight-bold">Travelgo</p>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Home</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Tentang</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Pelayanan</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Kontak</p>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="d-flex flex-column my-2 mx-2">
                      <div class="d-flex flex-row">
                        <div class="inline-block mx-3">
                          <p class="text-white font-weight-bold">Layanan</p>
                          <a href="#" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Hotel</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Pesawat</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Apartemen</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Kereta</p>
                          </a>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="d-flex flex-column my-2 mx-2">
                      <div class="d-flex flex-row">
                        <div class="inline-block mx-3">
                          <p class="text-white font-weight-bold">Event</p>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">TSF</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Game Koin</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">TSC</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Cashback</p>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="d-flex flex-column my-2 mx-2">
                      <div class="d-flex flex-row">
                        <div class="inline-block mx-3">
                          <p class="text-white font-weight-bold">Follow</p>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Instagram</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Facebook</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Twitter</p>
                          </a>
                          <a href="" style="text-decoration: none;">
                            <p class="text-white font-weight-light">Line</p>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- End Footer -->
            <script src="./js/jquery.slim.min.js"></script>
            <script src="./js/bootstrap.js"></script>
            <script src="./libs/fontawesome-free-6.2.0-web/js/fontawesome.js"></script>
            <script src="./libs/fontawesome-free-6.2.0-web/js/all.js"></script>

</body>

</html>